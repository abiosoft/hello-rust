extern crate gio;
extern crate glib;
extern crate gtk;

use gio::prelude::*;
use gio::ApplicationFlags;
use gtk::prelude::*;
use gtk::{Application, ApplicationWindow};

mod static_resource;
mod window;

fn main() {
  gtk::init().expect("Error initialising gtk");
  static_resource::init().expect("Error loading static resource");

  let app: Application = Application::new("org.gnome.HelloRust", ApplicationFlags::empty())
    .expect("Could not create application");

  app.connect_startup(setup_window);

  app.run(&std::env::args().collect::<Vec<_>>());
}

fn setup_window(app: &Application) {
  let w: ApplicationWindow = window::new_window();
  w.set_application(app);
  w.show_all();
}
